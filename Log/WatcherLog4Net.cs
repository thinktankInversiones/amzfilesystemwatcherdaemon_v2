﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using log4net;
using log4net.Core;
using log4net.Appender;
using log4net.Repository.Hierarchy;
using System.Reflection;
using System.IO;
using log4net.Layout;
using System.Xml;
using System.Text.RegularExpressions;
using WDLogger;

namespace AMZFileSystemWatcherDaemon_v2
{
    /// <summary>
    /// Clase encargada de la Gestión y configuración de Log4Net necesaria para todo el tratamiento de logs de la Aplicación
    /// Esta clase contiene los métodos para la inicialización, escritura o la generación de un fichero por día para los Logs de la Aplicación
    /// </summary>
    public class WatcherLog4Net
    {
        #region Atributes
        /// <summary>
        /// Indica si ya se realizó la inicializacion de log4net
        /// </summary>
        private bool isConfigured = false;
        /// <summary>
        /// objeto log4net utilizado para escribir los mensajees en el log
        /// </summary>
        private ILog iLog;
        /// <summary>
        /// nombre que se va a utilizar para identificar los ficheros a crear
        /// </summary>
        public String fileName { get; set; }
        /// <summary>
        /// directorio en el que se ubicaran los ficheros de log
        /// Es un path relativo a partir de Directory.GetCurrentDirectory()
        /// </summary>
        public String filePath { get; set; }
        /// <summary>
        /// contiene el formato de log a utilizar
        /// </summary>
        public WatcherRollingFileAppender appender;

        /// <summary>
        /// Nombre del objeto para el que genera el log
        /// </summary>
        public String objectName { get; set; }
        /// <summary>
        ///indica si el log a mostrar será o no critico
        /// </summary>
        public bool Critical { get; set; }
        /// <summary>
        ///indica si el log mostrará directamente el texto que recibe mas una fecha inicial (deberia tener formato XML ) ó escribe XML
        /// </summary>
        public bool FreeXML { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// constructor utilizado para inicializar los parametros que permiten utilizar el log.
        /// Se utiliza en el caso de necesitar añadir al LOG XML LIBRE
        /// </summary>
        /// <param name="name">nombre que se va a utilizar para identificar los ficheros a crear</param>
        /// <param name="pathCompleto">path completo en el que se ubicaran los ficheros de log</param>
        public WatcherLog4Net(String name, String pathCompleto)
        {
            objectName = String.Empty;
            FreeXML = true;
            fileName = name;
            filePath = pathCompleto + @"\";// +fileName + @"\";
            this.Critical = false;
            appender = new WatcherRollingFileAppender();// log4net.Appender.RollingFileAppender();
            Configure();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Realiza la inicializacion necesaria, para poder utilizar el objeto de log
        /// </summary>
        public void Configure()
        {
            if (isConfigured)
                return;
            var loggerName = this.fileName;// Environment.MachineName;//typeof(DiagnosticsManager).FullName;
            if (objectName == String.Empty)
            {
                objectName = this.fileName;
            }
            try
            {
                var logger = (log4net.Repository.Hierarchy.Logger)log4net.LogManager.GetRepository().GetLogger(loggerName);
                var ilogger = log4net.LogManager.GetRepository().GetLogger(loggerName);

                //Add the default log appender if none exist
                WatcherRollingFileAppender rfaFind = (WatcherRollingFileAppender)logger.GetAppender(objectName);
                if ((logger.Appenders.Count == 0)||(Object.ReferenceEquals(rfaFind,null)))
                {
                    ConfigureAppender();

                }
                else //ya existe el appender
                {
                    WatcherRollingFileAppender rfa =(WatcherRollingFileAppender)logger.GetAppender(objectName);
                    appender = rfa;
                    var directoryName = this.filePath;
                    //If the directory doesn't exist then create it
                    if (!Directory.Exists(directoryName))
                        Directory.CreateDirectory(directoryName);
                    var name = Path.Combine(directoryName, this.fileName);
                    //Create the rolling file appender
                    appender.Name = objectName;
                    appender.File = name;
                    //appender.ActivateOptions();

                }

                if (appender != null)
                {
                    appender.ActivateOptions();
                }
                iLog = LogManager.GetLogger(loggerName);
                //importante:: permite tener diferentes appender, si se utilizase (log4net.Config.BasicConfigurator.Configure(appender);) todos los logger compartirian una misma configuracion
                ((log4net.Repository.Hierarchy.Logger)iLog.Logger).AddAppender(appender);
                 isConfigured = true;
                Info("Logging Configured at " + DateTime.Now.ToString("g"));
            }
            catch(Exception e)
            {
               Fatal(e.Message,"GWLog4Net",e);
               throw e;
            }
        }

        private void ConfigureAppender()
        {
            var directoryName = this.filePath;

            //If the directory doesn't exist then create it
            if (!Directory.Exists(directoryName))
                Directory.CreateDirectory(directoryName);

            var name = Path.Combine(directoryName, this.fileName);

            //Create the rolling file appender
            if (objectName == String.Empty)
            {
                objectName = this.fileName;
            }
            appender.Name = objectName;// "RollingLogFileAppender";//this.fileName;// "RollingFileAppender";
            appender.File = name;
            //si StaticLogFileName==true, no se añade el datePattern al nombre del fichero
            appender.StaticLogFileName = false;
            appender.AppendToFile = true;
            appender.RollingStyle = log4net.Appender.RollingFileAppender.RollingMode.Date;
            appender.DatePattern = "yyyyMMdd'.txt'";//"'.'yyyyMMddHHmm";
            appender.MaxSizeRollBackups = -1;
            appender.MaximumFileSize = "1GB";
            appender.PreserveLogFileNameExtension = false;
            appender.Encoding = Encoding.UTF8;

            //Configure the layout of the trace message write
            var layout = new WatcherLayout();
            appender.Layout = layout;
            layout.ActivateOptions();

            //if (FreeXML)
            //{
            //    var layout = new log4net.Layout.PatternLayout()
            //    {
            //        ConversionPattern = "%message%newline"
            //        //ConversionPattern = "%property{ModuleGlobal}%property{DataGlobal}%property{Module}%property{Data}%message%newline"
            //    };
            //    appender.Layout = layout;
            //    layout.ActivateOptions();
            //}
            //else
            //{
            //    // Se utiliza un formato XML definido a traves de la clase GWXmlLayout
            //    var layout = new SQSLayout(Critical);
            //    appender.Layout = layout;
            //    layout.ActivateOptions();
            //}
        }
        //public static event EventHandler<ExceptionLoggedEventArgs> ExceptionLogged;
        /// <summary>
        /// Muestra en el log a nivel debug el valor del paramtero mensaje
        /// </summary>
        /// <param name="message">Texto informativo</param>
        public void Debug(object message, bool MuestraWDLogger=false) 
        { 
            if(MuestraWDLogger)
                Console.WriteLine(message.ToString());
            Configure();
            appender.Data = String.Empty;
            appender.Module = String.Empty; 
            iLog.Debug(message); 
        }
        /// <summary>
        /// Muestra en el log a nivel debug el valor de los parametros
        /// </summary>
        /// <param name="message">Texto informativo</param>
        /// <param name="module">Nombre del objeto que se esta ejecutando</param>
        /// <param name="data">Mensaje que se esta transmitiendo</param>
        public void Debug(object message, object module ,object data) { 
            Configure();
            appender.Module=module.ToString();
            appender.Data=data.ToString();
            iLog.Debug(message); }
        /// <summary>
        /// Muestra en el log a nivel debug el valor de los parametros
        /// </summary>
        /// <param name="message">Texto informativo</param>
        /// <param name="module">Nombre del objeto que se esta ejecutando</param>
        public void Debug(object message, object module)
        { 
            Configure();
            appender.Module=module.ToString();
            appender.Data = String.Empty;
            iLog.Debug(message); }
        /// <summary>
        /// Muestra en el log a nivel debug el valor de los parametros
        /// </summary>
        /// <param name="message">Texto informativo</param>
        /// <param name="exception">Excepcion cuyo mensaje se añade al log</param>
        public void Debug(object message, Exception exception) { Configure(); iLog.Debug(message, exception); }
        /// <summary>
        /// Muestra en el log a nivel error el valor de los parametros
        /// </summary>
        /// <param name="message">Texto informativo</param>
        public void Error(object message) 
        {
            WDLoggerUtil.AddError(message.ToString());
            Configure();
            appender.Data = String.Empty;
            appender.Module = String.Empty;
            iLog.Error(message); 
        }
        /// <summary>
        /// Muestra en el log a nivel error el valor de los parametros
        /// </summary>
        /// <param name="message">Texto informativo</param>
        /// <param name="module">Nombre del objeto que se esta ejecutando</param>
        /// <param name="data">Mensaje que se esta transmitiendo</param>
        /// <param name="exception">Excepcion cuyo mensaje se añade al log</param>
        public void Error(object message, object module, object data, Exception exception)
        {
            WDLoggerUtil.AddError(message.ToString(), exception);
            Configure();
            appender.Module = module.ToString();
            appender.Data = data.ToString();
            iLog.Error(message, exception);
        }
        /// <summary>
        /// Muestra en el log a nivel error el valor de los parametros
        /// </summary>
        /// <param name="message">Texto informativo</param>
        /// <param name="module">Nombre del objeto que se esta ejecutando</param>
        /// <param name="exception">Excepcion cuyo mensaje se añade al log</param>
        public void Error(object message, object module, Exception exception)
        {
            WDLoggerUtil.AddError(message.ToString(), exception);
            Configure();
            appender.Module = module.ToString();
            appender.Data = String.Empty;
            iLog.Error(message, exception);
        }

        /// <summary>
        /// Muestra en el log a nivel error el valor de los parametros
        /// </summary>
        /// <param name="message">Texto informativo</param>
        /// <param name="module">Nombre del objeto que se esta ejecutando</param>
        public void Error(object message, object module)
        {
            WDLoggerUtil.AddError(message.ToString());
            Configure();
            appender.Module = module.ToString();
            appender.Data = String.Empty;
            iLog.Error(message);
        }
        /// <summary>
        /// Muestra en el log a nivel error el valor de los parametros
        /// </summary>
        /// <param name="message">Texto informativo</param>
        /// <param name="module">Nombre del objeto que se esta ejecutando</param>
        /// <param name="data">Mensaje que se esta transmitiendo</param>
        public void Error(object message, object module, object data)
        {
            WDLoggerUtil.AddError(message.ToString());
            Configure();
            appender.Module = module.ToString();
            appender.Data = data.ToString();
            iLog.Error(message);
        }

        /// <summary>
        /// Muestra en el log a nivel fatal el valor de los parametros
        /// </summary>
        /// <param name="message">Texto informativo</param>
        public void Fatal(object message) {
            WDLoggerUtil.AddError(message.ToString());
            Configure();
            appender.Data = String.Empty;
            appender.Module = String.Empty; 
            iLog.Fatal(message); }
        /// <summary>
        /// Muestra en el log a nivel fatal el valor de los parametros
        /// </summary>
        /// <param name="message">Texto informativo</param>
        /// <param name="module">Nombre del objeto que se esta ejecutando</param>
        /// <param name="data">Mensaje que se esta transmitiendo</param>
        /// <param name="exception">Excepcion cuyo mensaje se añade al log</param>
        public void Fatal(object message, object module, object data, Exception exception)
        {
            WDLoggerUtil.AddError(message.ToString(), exception);
            Configure();
            appender.Module = module.ToString();
            appender.Data = data.ToString();
            iLog.Fatal(message, exception);
        }
        /// <summary>
        /// Muestra en el log a nivel fatal el valor de los parametros
        /// </summary>
        /// <param name="message">Texto informativo</param>
        /// <param name="module">Nombre del objeto que se esta ejecutando</param>
        /// <param name="exception">Excepcion cuyo mensaje se añade al log</param>
        public void Fatal(object message, object module, Exception exception)
        {
            WDLoggerUtil.AddError(message.ToString(), exception);
            Configure();
            appender.Module = module.ToString();
            appender.Data = String.Empty;
            iLog.Fatal(message, exception);
        }
        /// <summary>
        /// Muestra en el log a nivel info el valor de los parametros
        /// </summary>
        /// <param name="message">Texto informativo</param>
        public void Info(object message) { 
            Configure();
            appender.Data = String.Empty;
            appender.Module = String.Empty; 
            iLog.Info(message); }
        /// <summary>
        /// Muestra en el log a nivel info el valor de los parametros
        /// </summary>
        /// <param name="message">Texto informativo</param>
        /// <param name="module">Nombre del objeto que se esta ejecutando</param>
        /// <param name="data">Mensaje que se esta transmitiendo</param>
        public void Info(object message, object module, object data)
        {
            Configure();
            appender.Module = module.ToString();
            appender.Data = data.ToString();
            iLog.Info(message);
        }
        /// <summary>
        /// Muestra en el log a nivel info el valor de los parametros
        /// </summary>
        /// <param name="message">Texto informativo</param>
        /// <param name="module">Nombre del objeto que se esta ejecutando</param>
        public void Info(object message, object module)
        {
            Configure();
            appender.Module = module.ToString();
            appender.Data = String.Empty;
            iLog.Info(message);
        }
        /// <summary>
        /// Muestra en el log a nivel info el valor de los parametros
        /// </summary>
        /// <param name="message">Texto informativo</param>
        /// <param name="module">Nombre del objeto que se esta ejecutando</param>
        /// <param name="exception">Excepcion cuyo mensaje se añade al log</param>
        public void Info(object message, object module, Exception exception)
        {
            Configure();
            appender.Module = module.ToString();
            appender.Data = String.Empty;
            iLog.Info(message, exception);
        }

        /// <summary>
        /// Muestra en el log a nivel warn el valor de los parametros
        /// </summary>
        /// <param name="message">Texto informativo</param>
        public void Warn(object message) {
            WDLoggerUtil.AddWarning(message.ToString());
            Configure();
            appender.Data = String.Empty;
            appender.Module = String.Empty; 
            iLog.Warn(message); }
        /// <summary>
        /// Muestra en el log a nivel warn el valor de los parametros
        /// </summary>
        /// <param name="message">Texto informativo</param>
        /// <param name="module">Nombre del objeto que se esta ejecutando</param>
        /// <param name="data">Mensaje que se esta transmitiendo</param>
        /// <param name="exception">Excepcion cuyo mensaje se añade al log</param>
        public void Warn(object message, object module, object data, Exception exception)
        {
            WDLoggerUtil.AddWarning(message.ToString()+" "+exception.Message);
            Configure();
            appender.Module = module.ToString();
            appender.Data = data.ToString();
            iLog.Warn(message, exception);
        }
        /// <summary>
        /// Muestra en el log a nivel warn el valor de los parametros
        /// </summary>
        /// <param name="message">Texto informativo</param>
        /// <param name="module">Nombre del objeto que se esta ejecutando</param>
        /// <param name="exception">Excepcion cuyo mensaje se añade al log</param>
        public void Warn(object message, object module, Exception exception)
        {
            WDLoggerUtil.AddWarning(message.ToString() + " " + exception.Message);
            Configure();
            appender.Module = module.ToString();
            appender.Data = String.Empty;
            iLog.Warn(message, exception);
        }
        #endregion
    }

}