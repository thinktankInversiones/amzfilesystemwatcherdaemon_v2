﻿using log4net.Core;
using log4net.Layout;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace AMZFileSystemWatcherDaemon_v2
{
    /// <summary>
    /// Clase encargada de la Gestión de las Etiquetas utilizadas para la identificación del Tipo de Log
    /// </summary>
    public class WatcherLayout : log4net.Layout.PatternLayout
    {
        //public const string DefaultPattern = "%message%newline";
        //public const string DetailPattern = "%timestamp [%thread] %level %logger %ndc - %message%newline";
        public const string MyPattern = "%-5level %date{HH:mm:ss,fff} [%thread] %identity %logger %property{Module} %property{Data} - %message%newline";

        #region Constructors
        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public WatcherLayout()
        {
            //ConversionPattern = MyPattern;
        }
        #endregion

        public override void Format(TextWriter writer, LoggingEvent loggingEvent)
        {
            writer.Write(loggingEvent.Level.ToString().PadRight(5,' ')+ " ");
            writer.Write(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss fff")+" ");
            writer.Write("["+loggingEvent.ThreadName.PadLeft(5, ' ') + "] ");
            try
            {
                if (loggingEvent.Properties["module"].ToString() != String.Empty)
                {
                    writer.Write(loggingEvent.Properties["module"].ToString()+ " ");
                }
            }
            catch { };
            writer.Write(loggingEvent.RenderedMessage+" ");
            try
            {
                if (loggingEvent.Properties["data"].ToString() != String.Empty)
                {
                    writer.Write(loggingEvent.Properties["data"].ToString()+ " ");
                }
            }
            catch { };
            writer.WriteLine();

        }
    }  

}
