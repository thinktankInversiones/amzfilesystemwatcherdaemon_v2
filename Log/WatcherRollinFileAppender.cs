﻿using log4net.Appender;
using log4net.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AMZFileSystemWatcherDaemon_v2
{
    /// <summary>
    /// Clase encargada de la Gestión del RollingFileAppender que permite tener un fichero de Log por día para cada uno de los Canales
    /// </summary>
   public  class WatcherRollingFileAppender : RollingFileAppender
    {
       #region log4net
       [NonSerialized]
       private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
       #endregion
       
       #region Atributes
       /// <summary>
        /// Representa el elemento de Origen, Destino o Tarea
       /// </summary>
       public String Module { get; set; }
       /// <summary>
       /// Mensaje
       /// </summary>
       public String Data { get; set; }
       #endregion
       #region Methods

       /// <summary>
       /// Sobreescribe el evento de la clase padre para dar valor a las nuevas propiedades a mostras (Module y Data)
       /// </summary>
       /// <param name="e"></param>
       protected override void Append(LoggingEvent e)
        {
        var properties = e.Properties;

        properties["module"] = string.Empty;
        properties["data"] = string.Empty;

        try {
            properties["module"] = Module; }
        catch (Exception exc) { Logger.Error("Append Module", exc); }
        try { properties["data"] = Data; }
        catch (Exception exc) { Logger.Error("Append Data", exc); }
        base.Append(e);
        }
       #endregion
    }
}



