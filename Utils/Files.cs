﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace AMZFileSystemWatcherDaemon_v2
{
    public static class Files
    {
        /// <summary>
        /// Comprueba si un fichero esta disponible para su uso validando que ya haya finalizado su escritura, este siendo utilizado por otro proceso o no existe
        /// </summary>
        /// <param name="file">Informacion de fichero</param>
        /// <returns>True en caso de que el fichero exista y no este bloqueado</returns>
        public static bool IsFileLocked(FileInfo file, FileShare share=FileShare.None)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, share);
                //stream = file.OpenRead();
            }
            catch (IOException ex)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                }
            }

            return false;
        }

  
        public static void DeleteOldFiles(string path, string strDays)
        {
            try
            {
                double days = double.Parse(string.Format("-{0}", strDays));

                DateTime monthAgo = DateTime.Today.AddDays(days);

                foreach (string fileName in Directory.GetFiles(path))
                {
                    if (File.GetCreationTime(fileName) < monthAgo)
                    {
                        File.Delete(fileName);
                    }
                }
            }
            catch (Exception ex)
            {
                //LoggerTxt.AddEvent(string.Format("Error al eliminar ficheros antiguos en la carpeta {0}: {1}", path, ex.ToString()));
            }
        }

        public static void Move(string fullFileName, string destiny, WatcherLog4Net log)
        {
            try
            {
                if (!Directory.Exists(destiny))
                    Directory.CreateDirectory(destiny);

                File.Move(fullFileName, destiny + DateTime.Now.ToString("ddMMyyyyHHmmss")+"_" + Path.GetFileName(fullFileName));

            }
            catch (Exception ex)
            {
                log.Error("Files.Move " + ex.Message);
            }
        }

        public static int WriteToCsv<T>(IList<T> list, WatcherLog4Net log, string name, string[] propertyNames = null, string strSeparator = ";")
        {
            log.Debug(" ** Generando CSV: " + log.filePath + "csv_" + name);
            //Console.WriteLine(" ** Generando CSV: " + filePath);
            StringBuilder csv = new StringBuilder();
            //get property names from the first object using reflection    
            var propertyInfos = RelevantPropertyInfos<T>(propertyNames);
            IEnumerable<PropertyInfo> props = propertyInfos;// list.First().GetType().GetProperties();

            //header 
            csv.AppendLine(String.Join(strSeparator, props.Select(prop => prop.Name)));
            //rows
            foreach (var entityObject in list)
            {
                csv.AppendLine(String.Join(strSeparator, props.Select(
                    prop => modifiProperty((prop.GetValue(entityObject, null) ?? "").ToString())))
                );
            }
            //put a breakpoint here and check datatable

            System.IO.File.WriteAllText(log.filePath + "csv_"+name + DateTime.Now.ToString("yyyyMMddHHmmss")+".csv", csv.ToString(), Encoding.Default);
            return list.Count;
        }


        public static void WriteToLog<T>(IList<T> list, WatcherLog4Net log, string[] propertyNames = null, string strSeparator = ";")
        {
            //get property names from the first object using reflection    
            var propertyInfos = RelevantPropertyInfos<T>(propertyNames);
            IEnumerable<PropertyInfo> props = propertyInfos;// list.First().GetType().GetProperties();

            //header 
            log.Debug(String.Join(strSeparator, props.Select(prop => prop.Name)));
            //rows
            foreach (var entityObject in list)
            {
                log.Debug(String.Join(strSeparator, props.Select(
                    prop => modifiProperty((prop.GetValue(entityObject, null) ?? "").ToString())))
                );
            }
            log.Debug("_____________________________________________");
        }
        private static string modifiProperty(string v)
        {
            string replacement = Regex.Replace(v, @"\t|\n|\r", "");
            replacement = Regex.Replace(replacement, @";", ".");
            //replacement = Regex.Replace(replacement, @",", " ");
            return replacement;
        }

        private static List<PropertyInfo> RelevantPropertyInfos<T>(IEnumerable<string> propertyNames = null)
        {
            if (!(object.ReferenceEquals(propertyNames, null)) && propertyNames.Count() > 0)
            {
                var propertyInfos = typeof(T).GetProperties().Where(p => propertyNames.Contains(p.Name)).ToDictionary(pi => pi.Name, pi => pi);
                return (from propertyName in propertyNames where propertyInfos.ContainsKey(propertyName) select propertyInfos[propertyName]).ToList();
            }
            else
            {
                var propertyInfos = typeof(T).GetProperties();
                return propertyInfos.ToList();
            }


        }

        public static void AddOldFiles(string dirOrigen, string Destinypath, WatcherLog4Net log)
        {
            //string Destinypath = ConfigurationManager.AppSettings["ProductLoadsPendingFilePath"].ToString();
            // string[] newFiles = Directory.GetFiles(dir);
            DirectoryInfo dir = new DirectoryInfo(dirOrigen);
            FileSystemInfo[] files = dir.GetFileSystemInfos();
            var orderedFiles = files.OrderBy(f => f.CreationTime);
            log.Debug(string.Format("Se aniaden {0} ficheros pendientes a:{1}", orderedFiles.Count(), Destinypath), true);
            foreach (FileSystemInfo file in orderedFiles)
            {
                string fileName = Path.GetFileName(file.FullName);
                File.Move(file.FullName, Destinypath + "\\" + fileName);
            }
        }

        public static string MoveOldFiles(string folderName, WatcherLog4Net log)
        {
            string path = ConfigurationManager.AppSettings["ProductLoadsPendingFilePath"].ToString();
            string[] OldFiles = Directory.GetFiles(path);
            DirectoryInfo previousDir = Directory.GetParent(Directory.GetParent(path).ToString());
            string destinyPath= previousDir + "\\"+ folderName + "\\";
            if (!Directory.Exists(destinyPath))
                Directory.CreateDirectory(destinyPath);
            log.Debug(string.Format("Se mueven {0} ficheros pendientes a:{1}", OldFiles.Count(), destinyPath), true);
            foreach (string file in OldFiles)
            {
                string fileName = Path.GetFileName(file);
                //if (!FileInProccess(fileName))
                    File.Move(file, destinyPath + fileName);
            }
            return previousDir.FullName + "\\"+ folderName;
        }

        public static bool FileInProccess(string fileName)
        {

            return ((fileName.StartsWith(DateTime.Now.ToString("ddMMyyyy"))) || (fileName.StartsWith(DateTime.Now.AddDays(-1).ToString("ddMMyyyy"))));
        }

        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                }
            }

            return false;
        }

        /// <summary>
        /// Carga en memoria un fichero con un nombre y ruta determinados
        /// </summary>
        /// <returns></returns>
        public static StreamReader LoadFile(string filePath)
        {
            StreamReader sr = null;

            try
            {
                sr = new StreamReader(filePath, System.Text.Encoding.Default);
            }
            catch (FileNotFoundException)
            {
                throw new Exception("El fichero de origen no existe en la ubicacion proporcionada");
            }
            catch (DirectoryNotFoundException)
            {
                throw new Exception("La ruta al fichero de origen no existe");
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el manejo del fichero: " + ex.Message);
            }

            return sr;
        }

    }
}
