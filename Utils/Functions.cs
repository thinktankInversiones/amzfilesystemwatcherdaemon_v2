﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using WDLogger;

namespace AMZFileSystemWatcherDaemon_v2.Utils
{
    public static class Functions
    {
        public static void SendMailErrors(string fileName, string mensajeError)
        {
            List<string> errorsToSend = new List<string>();
            StringBuilder mensaje = new StringBuilder();
            mensaje.AppendLine(" Errores abriendo proceso :" + fileName);
            mensaje.AppendLine("");
            mensaje.AppendLine(mensajeError);
            mensaje.AppendLine("");
            string[] destino = ConfigurationManager.AppSettings["EmailNotificacion"].Split(';');
            string origen = ConfigurationManager.AppSettings["EmailOrigen"];
            if (AmazonRepricingClasses.Utils.Functions.SendMail("Errores AMZ procesado de fichero " + fileName, mensaje.ToString(), false, origen, null, destino.ToList()))
                WDLoggerUtil.AddLog(string.Format("Enviado Mail de Errores"));
            else
                WDLoggerUtil.AddLog(string.Format("Error al intentar anviar Mail de Errores"));

        }
    }
}
