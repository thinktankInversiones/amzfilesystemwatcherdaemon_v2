﻿using AmazonRepricingClasses.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using WDLogger;

namespace AMZFileSystemWatcherDaemon_v2
{
    class Program
    {
        private static FileSystemWatcher productLoadsWatcher;
        private static WatcherLog4Net log;
        private static Watcher.WatcherHandler handler;
        private static System.Timers.Timer timer;
        private static System.Timers.Timer timerStatus;
        private static List<string> firstLine;
        static void Main(string[] args)
        {
            int idAplication = int.Parse(ConfigurationManager.AppSettings["idAplication"]);

            WDLoggerUtil.StartExecution(idAplication);
            WDLoggerUtil.AddLog("Init AmzFileSystemWatcherDaemon_v2 " + DateTime.Now.ToString("g"));
            log = new WatcherLog4Net("AmzFileSystemWatcherDaemon_v2", ConfigurationManager.AppSettings["RutaLog"]);
            //ERRORLog4Net log = new ERRORLog4Net("ErrorHandler", @"c:\Procesos\AMZ\ErrorHandler\log");
            Console.Title = string.Format("AmzFileSystemWatcherDaemon_v2 - {0}", DateTime.Now.ToString("g"));
            InitTimers();

            try
            {

                string strDays = ConfigurationManager.AppSettings["KeepCopyMaxDays"];
                string logPath = ConfigurationManager.AppSettings["RutaLog"].ToString();
                log.Debug("Borrado de ficheros antiguos en [" + logPath + "]", true);
                Files.DeleteOldFiles(logPath, strDays);
                log.Error("Llamada a proceso con [" + args.Length + "] parametros [" + string.Join(",", args) + "]", true);

                string dirOrigin = Files.MoveOldFiles("anteriores", log);
                //Se inicializa el manejador ficheros
                handler = new Watcher.WatcherHandler(int.Parse(ConfigurationManager.AppSettings["maxConcurrentExec"].ToString()), log);
                // Se pone a la escucha en la carpeta de cargas de ficheros CSV
                ProductLoadsWatch();

                Files.AddOldFiles(dirOrigin, ConfigurationManager.AppSettings["ProductLoadsPendingFilePath"].ToString(), log);

                log.Debug("Pulsa cualquier tecla para finalizar el proceso", true);
                //log.Info("Pulsa ENTER para finalizar el proceso");
                Console.Read();


            }
            catch (Exception ex)
            {
                log.Error("Error ", "Main", ex);
            }
            WDLoggerUtil.AddLog("END AmzFileSystemWatcherDaemon_v2 " + DateTime.Now.ToString("g"));
            WDLoggerUtil.EndExecution();
        }

        private static void InitTimers()
        {
            timer = new System.Timers.Timer();
            timer.Elapsed += OnTimedEvent;// new ElapsedEventHandler(updateHandelrStatus());
            timer.Interval = int.Parse(ConfigurationManager.AppSettings["TimerHandler"]) * 1000;
            timer.Enabled = true;

            timerStatus = new System.Timers.Timer();
            timerStatus.Elapsed += OnTimedStatusEvent;// new ElapsedEventHandler(updateHandelrStatus());
            timerStatus.Interval = int.Parse(ConfigurationManager.AppSettings["TimerStatusHandler"]) * 1000;
            timerStatus.Enabled = true;
        }

        /// <summary>
        /// Se comprueba si terminó alguno de los objetos que ocupan el proceso para si existe algun
        /// fichero encolado, lanzarlo.
        /// </summary>
        private static void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            if (handler != null)
            {
                log.Debug("TimerHandler: Comprobando estado de la cola");
                handler.updateProccessStatus();
            }
        }


        /// <summary>
        /// Se comprueba si terminó alguno de los objetos que ocupan el proceso para si existe algun
        /// fichero encolado, lanzarlo.
        /// </summary>
        private static void OnTimedStatusEvent(object sender, ElapsedEventArgs e)
        {
            if (handler != null)
            {
                log.Debug("TimerStatusHandler: Impromiendo estado de la cola");
                handler.printStatus();
            }
        }



        private static void ProductLoadsWatch()
        {
            productLoadsWatcher = new FileSystemWatcher();

            // Se indica la ruta sobre la que se desea escuchar la creacion de nuevos ficheros
            productLoadsWatcher.Path = ConfigurationManager.AppSettings["ProductLoadsPendingFilePath"].ToString();

            // Se indican las extensiones de archivos que nos interesan
            productLoadsWatcher.Filter = "*.*";

            // Se indica que no es necesario que escuche tambien en los subdirectorios de la ruta raiz proporcionada
            productLoadsWatcher.IncludeSubdirectories = false;

            // Se asocia el evento de creacion a un manejador propio
            productLoadsWatcher.Created += new FileSystemEventHandler(ProductLoadsOnChanged);

            productLoadsWatcher.EnableRaisingEvents = true;
        }

        private static void ProductLoadsOnChanged(object source, FileSystemEventArgs e)
        {
            // Se obtiene la ruta donde se produjo el evento mas el nombre del fichero
            string filePath = e.FullPath;


            FileInfo fileInfo = new FileInfo(filePath);

            // Se comprueba si el fichero esta disponible para su procesado
            while (Files.IsFileLocked(fileInfo))
            {
                Thread.Sleep(500);
            }

            log.Debug("Recibido Fichero: " + filePath, true);

            //mover el fichero al directorio correcto en funcion del providerInFile
            string mensaje = string.Empty;
            string fileToListen=string.Empty;
            bool ficheroEnUso = true;
            int numtry = 0;
            while ((ficheroEnUso)&&(numtry<5))
            {
                numtry++;
                fileToListen = changeFileLocation(filePath, out mensaje);
                if (mensaje.ToUpper().Contains("EL PROCESO NO PUEDE OBTENER ACCESO AL ARCHIVO"))
                {
                    ficheroEnUso = true;
                    Thread.Sleep(60000);
                }
                else
                    ficheroEnUso = false;
            }
            //Gestionar lista
            if (!string.IsNullOrEmpty(fileToListen))
            {
                handler.updateProccessStatus();
                handler.addProccess(fileToListen);
            }
            else
            {
                SendMailErrors(Path.GetFileName(filePath),mensaje);

            }

        }

        private static string changeFileLocation(string filePath, out string mensaje)
        {
            try
            {
                mensaje = string.Empty;
                string path = ConfigurationManager.AppSettings["ProductLoadsPendingFilePath"].ToString();
                DirectoryInfo previousDir = Directory.GetParent(Directory.GetParent(path).ToString());
                string providerName = GetProviderNameFromFile(filePath);
                if (string.IsNullOrEmpty(providerName))
                {
                    string errorPath = previousDir + "\\Error\\";
                    if (!Directory.Exists(errorPath))
                        Directory.CreateDirectory(errorPath);
                    string errorFilePath = errorPath +  "\\" + Path.GetFileName(filePath);
                    if (System.IO.File.Exists(errorFilePath))
                    {
                        File.Copy(filePath, errorFilePath, true);
                        File.Delete(filePath);
                    }
                    else
                        File.Move(filePath, errorFilePath);

                    log.Debug("ChangeFileLocation no existe nombre de proveedor");
                    mensaje = "No existe nombre de proveedor en las lineas del fichero";
                    return string.Empty;
                }
                string destinyPath = previousDir + "\\PendingWatcher\\";
                if (!Directory.Exists(destinyPath))
                    Directory.CreateDirectory(destinyPath);
                
                string newFolderPath = destinyPath + "\\" + providerName;
                string newFilePath = destinyPath + "\\" + providerName + "\\" + Path.GetFileName(filePath);
                if (!Directory.Exists(newFolderPath))
                    Directory.CreateDirectory(newFolderPath);
                if (System.IO.File.Exists(newFilePath))
                {
                    File.Copy(filePath, newFilePath, true);
                    File.Delete(filePath);
                }
                else
                    File.Move(filePath, newFilePath);
                return newFilePath;
            }
            catch (Exception ex)
            {
                log.Error("Error changeFileLocation "+ex.Message);
                mensaje = "Error: " + ex.Message;
                return null;
            }


        }

        /// <summary>
        /// Localiza el nombre del proveedor incluido en un fichero con el siguiente formato:
        /// Ean13;Nombre;Descripcion;Precio sin iva;Stock;Url portada;% Margen comercial;Proveedor;Peso(kg);% IVA
        /// </summary>
        /// <param name="filePath">Ruta donde se encuentra el fichero</param>
        /// <returns>Nombre del proveedor</returns>
        private static string GetProviderNameFromFile(string filePath)
        {
            string providerName = string.Empty;
            if (firstLine == null)
            {
                firstLine = new List<string>();
            }
            else
            {
                firstLine.Clear();
            }
            using (StreamReader sr = Files.LoadFile(filePath))
            {

                if (sr != null)
                {
                    try
                    {
                        string line = sr.ReadLine();// Se pasa sobre la linea de cabecera

                        line = sr.ReadLine();

                        string[] data = line.Split(char.Parse(ConfigurationManager.AppSettings["ColumnDividerCharacter"].ToString()));

                        firstLine.AddRange(data.ToList());

                        int providerColumnPosition = int.Parse(ConfigurationManager.AppSettings["ProviderColumnPosition"]);

                        providerName = data[providerColumnPosition].Trim();

                        // Se eliminan las comillas dobles incluidas en algunos de los ficheros de productos provinientes de Toad Data Point
                        providerName = providerName.Replace('\"', ' ').Trim();

                        sr.Close();
                    }
                    catch { }
                }
            }
            return providerName;
        }


        private static void SendMailErrors(string fileName, string mensajeError)
        {
            List<string> errorsToSend = new List<string>();
            StringBuilder mensaje = new StringBuilder();
            mensaje.AppendLine(" Errores en fichero :" + fileName);
            mensaje.AppendLine("");
            mensaje.AppendLine(mensajeError);
            try
            {
                if (firstLine != null && firstLine.Count() > 0)
                {
                    mensaje.AppendLine("La linea inicial del fichero contiene los siguientes datos");
                    for (int i = 0; i < firstLine.Count(); i++)
                    {
                        mensaje.AppendLine(string.Format("Columna {0} = {1}", i + 1, firstLine[i]));
                    }
                }
                else
                {
                    mensaje.AppendLine("");
                    mensaje.AppendLine("La linea inicial del fichero no contiene información, fichero vacio");
                }
            }
            catch (Exception ex) { }
            mensaje.AppendLine("");
            mensaje.AppendLine("Formato necesario del fichero");
            mensaje.AppendLine("Ean13 Nombre Descripcion PrecioSinIva Stock UrlPortada MargenComercial Proveedor Peso IVA");
            mensaje.AppendLine("columna 1-A: Ean13");
            mensaje.AppendLine("columna 2-B: Nombre / Titulo");
            mensaje.AppendLine("columna 3-C: Descripcion");
            mensaje.AppendLine("columna 4-D: Precio");
            mensaje.AppendLine("columna 5-E: stock");
            mensaje.AppendLine("columna 6-F: URLPortada");
            mensaje.AppendLine("columna 7-G: Margen");
            mensaje.AppendLine("columna 8-H: Proveedor");
            mensaje.AppendLine("columna 9-I: Peso");
            mensaje.AppendLine("columna 10-J: IVA");
            string[] destino = ConfigurationManager.AppSettings["EmailNotificacion"].Split(';');
            string origen = ConfigurationManager.AppSettings["EmailOrigen"];
            if (Functions.SendMail("Errores AMZ lectura de fichero " + fileName, mensaje.ToString(), false, origen, null, destino.ToList()))
                WDLoggerUtil.AddLog(string.Format("Enviado Mail de Errores"));
            else
                WDLoggerUtil.AddLog(string.Format("Error al intentar anviar Mail de Errores"));

        }

    }
}
