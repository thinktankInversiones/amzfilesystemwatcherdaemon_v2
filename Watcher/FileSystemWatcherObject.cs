﻿using AmazonRepricingClasses.BLL;
using AmazonRepricingClasses.DTO;
using AMZFileSystemWatcherDaemon_v2.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using WDLogger;

namespace AMZFileSystemWatcherDaemon_v2.Watcher
{
    public class FileSystemWatcherObject
    {
        public string Path { get; set; }
        public FileSystemWatcher productLoadsWatcher { get; set; }

        public static WatcherLog4Net log { get; set; }

        private bool eventHandled;
        private Process myProcess;

        public FileSystemWatcherObject(string pathName, WatcherLog4Net logg)
        {
            eventHandled = false;
            log = logg;
            Path = pathName;
            //InitProductLoadsWatch();
        }

        public void InitProductLoadsWatch()
        {
            productLoadsWatcher = new FileSystemWatcher();

            // Se indica la ruta sobre la que se desea escuchar la creacion de nuevos ficheros
            productLoadsWatcher.Path = Path;

            // Se indican las extensiones de archivos que nos interesan
            productLoadsWatcher.Filter = "*.*";

            // Se indica que no es necesario que escuche tambien en los subdirectorios de la ruta raiz proporcionada
            productLoadsWatcher.IncludeSubdirectories = false;

            // Se asocia el evento de creacion a un manejador propio
            productLoadsWatcher.Created += new FileSystemEventHandler(ProductLoadsOnChanged);

            productLoadsWatcher.EnableRaisingEvents = true;
        }

        private void ProductLoadsOnChanged(object source, FileSystemEventArgs e)
        {
            // Se obtiene la ruta donde se produjo el evento mas el nombre del fichero
            string filePath = e.FullPath;


            FileInfo fileInfo = new FileInfo(filePath);

            // Se comprueba si el fichero esta disponible para su procesado
            while (Files.IsFileLocked(fileInfo))
            {
                Thread.Sleep(500);
            }

            log.Debug("Inicio procesado fichero "+filePath, true);

            LaunchApplication(filePath);

        }
        private void LaunchApplication(string filePath)
        {
            FileInfo fileInfo = new FileInfo(filePath);
            long fileSize = fileInfo.Length;

            try
            {
                string fileName = filePath.Substring(filePath.LastIndexOf('\\') + 1);

                // Se incluye un prefijo para conseguir un nombre de fichero unico y evitar problemas con ficheros con el mismo nombre
                string newfileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + "_" + fileName;

                // Se comprueba si es una carga manual (si dispone de registro en la tabla pending_product_loads) 
                // para en su caso actualizar el nombre del fichero al nuevo generado en la linea anterior
                ProductLoad productLoad = new ProductLoadBLL().Get(fileName);

                if (productLoad != null)
                {
                    productLoad.FileName = newfileName;

                    new ProductLoadBLL().Update(productLoad);
                }


                newfileName = newfileName.Replace(' ', '_');
                File.Move(filePath, Path + newfileName);

                //string arguments = string.Format("\"{0}\"", newfileName);
                string appPath = ConfigurationManager.AppSettings["ExecutablePath"].ToString();

                ExecuteProcess(newfileName, appPath, fileSize);

            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR <LaunchApplication>: " + ex.Message);
            }
        }

        private void ExecuteProcess(string newfileName, string appPath, long fileSizeBytes)
        {
            int elapsedTime;
           
            string arguments = string.Format("\"{0}\"", newfileName);

            elapsedTime = 0;
            eventHandled = false;
            bool iniciadoProceso = false;
            int contReintentos = 0;
            while ((contReintentos<5)&& (!iniciadoProceso))
            {
                try
                {
                    contReintentos++;
                    myProcess = new Process();
                    // Start a process to print a file and raise an event when done.
                    myProcess.StartInfo.FileName = appPath;
                    //myProcess.StartInfo.Verb = "Print";
                    myProcess.StartInfo.UseShellExecute = true;
                    myProcess.StartInfo.CreateNoWindow = true;
                    myProcess.StartInfo.ErrorDialog = false;
                    myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                    myProcess.StartInfo.Arguments = arguments;
                    myProcess.EnableRaisingEvents = true;

                    Stopwatch timer = new Stopwatch();
                    timer.Start();

                    myProcess.Exited += new EventHandler((sender, e) => myProcess_Exited(sender, e, timer, newfileName));
                    iniciadoProceso = myProcess.Start();

                }
                catch (Exception ex)
                {
                    log.Debug(string.Format("{0}(reintento) An error occurred \"{1}\":" + "\n{2}", contReintentos, appPath, ex.Message), true);
                    //return;
                    //se espera a ver si puede arrancar el proceso dentro de 1 minuto
                    if (contReintentos < 4)
                    {
                        log.Debug(string.Format("Esperamos 1 minuto para intentar de nuevo abrir {0} con args={1}", appPath, arguments), true);
                        Thread.Sleep(60000);
                    }
                    else
                    {
                        //se notifica el problema y se sale del fichero (se marca como procesado para ejecutar el siguiente)
                        Functions.SendMailErrors(string.Format("{0} con args={1}", appPath, arguments), ex.Message);
                        Stopwatch timer = new Stopwatch();
                        timer.Start();
                        exitProcess(timer, newfileName);
                    }
                }
            }

            // Wait for Exited event
            const int SLEEP_AMOUNT = 100;
            int MARGINTIMESECONDS = int.Parse(System.Configuration.ConfigurationManager.AppSettings["MARGINTIMESECONDS"]);
            while (!eventHandled)
            {
                elapsedTime += SLEEP_AMOUNT;
                if (elapsedTime > (fileSizeBytes + (MARGINTIMESECONDS * 1000)))
                {
                  // De momento se deshabilita la ejecucion en paralelo desde aqui.
                  //  break;
                }
                Thread.Sleep(SLEEP_AMOUNT);
            }
        }

        private void myProcess_Exited(object sender, System.EventArgs e, Stopwatch timer, string file)
        {
            exitProcess(timer, file);

        }

        private void exitProcess(Stopwatch timer, string file)
        {
            int sleepTime = 5000;
            bool WriteLog = false;
            eventHandled = true;
            timer.Stop();

            TimeSpan executionDuration = timer.Elapsed;
            while (!WriteLog)
            {
                try
                {
                    log.Debug(string.Format("Resultado {0}: ExitCode[{1}] Tiempo: {2}:{3}:{4}", file, myProcess.ExitCode, executionDuration.Hours, executionDuration.Minutes, executionDuration.Seconds), true);
                    //LoggerTxt.AddEvent(string.Format("Tiempo de ejecucion: {0}:{1}:{2}", executionDuration.Hours, executionDuration.Minutes, executionDuration.Seconds), true);
                    WriteLog = true;
                }
                catch (Exception ex)
                {
                    //Console.WriteLine(string.Format("Esperando Acceso a log 1 ({0})segundos {1}", sleepTime, ex.Message));
                    try
                    {
                        log.Debug(string.Format("Resultado {0}: Tiempo: {1}{2}:{3}", file, executionDuration.Hours, executionDuration.Minutes, executionDuration.Seconds), true);
                        WriteLog = true;
                    }
                    catch (Exception exInt)
                    {
                        Console.WriteLine(string.Format("Esperando Acceso a log 2 ({0})segundos {1}", sleepTime, exInt.Message));
                        Thread.Sleep(sleepTime);
                    }
                }
            }
        }
    }
}
