﻿using AmazonRepricingClasses.BLL;
using AmazonRepricingClasses.DTO;
using AMZFileSystemWatcherDaemon_v2.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AMZFileSystemWatcherDaemon_v2.Watcher
{
    public class WatcherObject
    {
        public string PendingPath { get; set; }
        public Queue<string> productLoadsWatcher { get; set; }
        public List<string> inExec { get; set; }
        public bool endProccess { get; set; }

        public DateTime fechaInicioProcesado { get; set; }
        public int PID { get; set; }
        public int instanciasAbiertas { get; set; }
        public int instanciasFraccionados { get; set; }
        public static WatcherLog4Net log { get; set; }

        private bool eventHandled;
        private Process myProcess;

        public WatcherObject(string pathName, WatcherLog4Net logg)
        {
            instanciasAbiertas = 1;
            instanciasFraccionados = 0;
            eventHandled = false;
            endProccess = false;
            log = logg;
            PendingPath = pathName;
            inExec = new List<string>();
            InitProductLoadsWatch();
        }

        public void InitProductLoadsWatch()
        {
            productLoadsWatcher = new Queue<string>();
        }
        public void AddProductLoadsWatch(string fullFileName, List<WatcherHistory> watcherHistory)
        {
            //productLoadsWatcher.Enqueue(Path.GetFileName(fullFileName));
            if (!productLoadsWatcher.Any(i => i == fullFileName))
            {
                //if (!checkFileLoadingEqual(fullFileName))
                //{
                    int numHoras = int.Parse(System.Configuration.ConfigurationManager.AppSettings["NumHorasControl"]);
                    if (!(existsFileLoadingProcessed(fullFileName, watcherHistory, numHoras)))
                    {
                        watcherHistory.Add(new WatcherHistory(fullFileName));
                        productLoadsWatcher.Enqueue(fullFileName);

                    }
                    else
                    {
                        log.Debug("Fichero ya procesado hace menos de "+ numHoras + " horas " + fullFileName);
                        Files.Move(fullFileName, Directory.GetParent(fullFileName).ToString() + "\\Saturate\\", log);

                    }
                //}
                //else
                //{
                //    log.Debug("Fichero igual a uno anterior " + fullFileName);
                //    Files.Move(fullFileName, Directory.GetParent(fullFileName).ToString() + "\\Repeat\\", log);
                //}
            }
            else
                log.Debug("Fichero anterior aún en cola " + fullFileName);
        }

        /// <summary>
        /// Un fichero se considera igual solamente si tiene la misma fecha y el mismo tamaño
        /// </summary>
        /// <param name="fullFileName"></param>
        /// <returns></returns>
        private bool checkFileLoadingEqual(string fullFileName)
        {
            try
            {
                FileInfo fileInfo = new FileInfo(fullFileName);
                long fileSize = fileInfo.Length;

                string loadingfileName=string.Empty;
                string[] OldFiles = Directory.GetFiles(Directory.GetParent(fullFileName).ToString());
                foreach (string file in OldFiles)
                {
                    string fileName = Path.GetFileName(file);
                    if (Files.FileInProccess(fileName))
                    {
                        loadingfileName = file;
                        break;
                    }
                }

                if (!string.IsNullOrEmpty(loadingfileName))
                {
                    FileInfo loadingfileInfo = new FileInfo(loadingfileName);
                    long loadingfileSize = loadingfileInfo.Length;
                    if (loadingfileSize == fileSize)
                        return true;
                }
            }
            catch (Exception ex)
            {
                log.Error("checkFileLoadingEqual " + fullFileName);
            }
            return false;
        }

        private bool existsFileLoadingProcessed(string fullFileName, List<WatcherHistory> watcherHistory, int NumHorasControl)
        {
            try
            {
                DateTime fechaControl = DateTime.Now.AddHours(-NumHorasControl);
                //si cambia de dia no hace el control de horas.
                if (fechaControl.Day != DateTime.Now.Day)
                    return false;
                List<WatcherHistory> listProcessed = watcherHistory.Where(o => o.Date >= fechaControl && o.FileName == fullFileName).ToList();
                if (listProcessed.Count() > 0)
                    return true;
            }
            catch (Exception ex)
            {
                log.Error("checkFileLoadingProcessed " + fullFileName+ " "+ex.Message);
            }
            return false;
        }


        public void executeNext()
        {
            if (productLoadsWatcher.Count > 0)
            {
                fechaInicioProcesado = DateTime.Now;
                string fullFileName = productLoadsWatcher.Dequeue();
                ProductLoadsOnChanged(fullFileName);
            }
            else
            {
                log.Debug(PendingPath + " sin ficheros pendientes");
                endProccess = true;
            }
        }

        //public void RemoveProductLoadsWatch(string fullFileName)
        //{
        //    productLoadsWatcher.Dequeue(Path.GetFileName(fullFileName));
        //}

        public bool isFreeProductLoadsWatch()
        {
            return (endProccess &&(productLoadsWatcher.Count == 0)) ? true : false;
        }

        private void ProductLoadsOnChanged(string fullFileName)
        {
            // Se obtiene la ruta donde se produjo el evento mas el nombre del fichero
            string filePath = fullFileName;

            FileInfo fileInfo = new FileInfo(filePath);

            // Se comprueba si el fichero esta disponible para su procesado
            while (Files.IsFileLocked(fileInfo))
            {
                Thread.Sleep(500);
            }

            log.Debug("Inicio procesado fichero " + filePath, true);

       
                
              LaunchApplication(filePath);

        }
        private void LaunchApplication(string filePath)
        {
            FileInfo fileInfo = new FileInfo(filePath);
            long fileSize = fileInfo.Length;

            try
            {
                inExec.Add(filePath);
                string fileName = filePath.Substring(filePath.LastIndexOf('\\') + 1);

                // Se incluye un prefijo para conseguir un nombre de fichero unico y evitar problemas con ficheros con el mismo nombre
                string newfileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + "_" + fileName;

                //// Se comprueba si es una carga manual (si dispone de registro en la tabla pending_product_loads) 
                //// para en su caso actualizar el nombre del fichero al nuevo generado en la linea anterior
                //ProductLoad productLoad = new ProductLoadBLL().Get(fileName);

                //if (productLoad != null)
                //{
                //    productLoad.FileName = newfileName;

                //    new ProductLoadBLL().Update(productLoad);
                //}


                newfileName = newfileName.Replace(' ', '_');
                string fullFileName = PendingPath + "\\" + newfileName;
                File.Move(filePath, fullFileName);

                //string arguments = string.Format("\"{0}\"", newfileName);
                string appPath = ConfigurationManager.AppSettings["ExecutablePath"].ToString();

                //Task taskA = Task.Factory.StartNew(() => ExecuteProcess(fullFileName, appPath, fileSize));

                new Thread(() =>
                {
                    ExecuteProcess(fullFileName, appPath, fileSize);
                }).Start();

            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR <LaunchApplication>: " + ex.Message);
            }
        }

        private void ExecuteProcess(string newfileName, string appPath, long fileSizeBytes)
        {
            int elapsedTime;

            string arguments = string.Format("\"{0}\"", newfileName);

            elapsedTime = 0;
            eventHandled = false;
            bool iniciadoProceso = false;
            int contReintentos = 0;
            while ((contReintentos < 5) && (!iniciadoProceso))
            {

                try
                {
                    myProcess = new Process();
                    // Start a process to print a file and raise an event when done.
                    myProcess.StartInfo.FileName = appPath;
                    //myProcess.StartInfo.Verb = "Print";
                    myProcess.StartInfo.UseShellExecute = true;
                    myProcess.StartInfo.CreateNoWindow = true;
                    myProcess.StartInfo.ErrorDialog = false;
                    myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                    myProcess.StartInfo.Arguments = arguments;
                    myProcess.EnableRaisingEvents = true;
                    //se almacena el PID del proceso para comprobar que no se mate.
                    Stopwatch timer = new Stopwatch();
                    timer.Start();

                    myProcess.Exited += new EventHandler((sender, e) => myProcess_Exited(sender, e, timer, newfileName));
                    iniciadoProceso = myProcess.Start();
                    PID = myProcess.Id;

                }
                catch (Exception ex)
                {
                    log.Debug(string.Format("{0}(reintento) An error occurred \"{1}\":" + "\n{2}",contReintentos, appPath, ex.Message), true);
                    //return;
                    if (contReintentos < 4)
                    {
                        log.Debug(string.Format("Esperamos 1 minuto para intentar de nuevo abrir {0} con args={1}", appPath, arguments), true);
                        Thread.Sleep(60000);
                    }
                    else
                    {
                        //se notifica el problema y se sale del fichero (se marca como procesado para ejecutar el siguiente)
                        Functions.SendMailErrors(string.Format("{0} con args={1}", appPath, arguments), ex.Message);
                        Stopwatch timer = new Stopwatch();
                        timer.Start();
                        exitProcess(timer, newfileName);
                    }
                }
            }

            // Wait for Exited event
            const int SLEEP_AMOUNT = 100;
            int MARGINTIMESECONDS = int.Parse(System.Configuration.ConfigurationManager.AppSettings["MARGINTIMESECONDS"]);
            while (!eventHandled)
            {
                elapsedTime += SLEEP_AMOUNT;
                if (elapsedTime > (fileSizeBytes + (MARGINTIMESECONDS * 1000)))
                {
                  // De momento se deshabilita la ejecucion en paralelo desde aqui.
                  //  break;
                }
                Thread.Sleep(SLEEP_AMOUNT);
            }
        }

        private void myProcess_Exited(object sender, System.EventArgs e, Stopwatch timer, string file)
        {
            exitProcess(timer, file);
        }

        private void exitProcess(Stopwatch timer, string file)
        {
            int sleepTime = 5000;
            bool WriteLog = false;
            eventHandled = true;
            timer.Stop();
            inExec.Remove(file);
            TimeSpan executionDuration = timer.Elapsed;
            //            RemoveProductLoadsWatch(file);
            executeNext();

            while (!WriteLog)
            {
                try
                {
                    log.Debug(string.Format("Resultado {0}: ExitCode[{1}] Tiempo: {2}:{3}:{4}", file, myProcess.ExitCode, executionDuration.Hours, executionDuration.Minutes, executionDuration.Seconds), true);
                    //LoggerTxt.AddEvent(string.Format("Tiempo de ejecucion: {0}:{1}:{2}", executionDuration.Hours, executionDuration.Minutes, executionDuration.Seconds), true);
                    WriteLog = true;
                }
                catch (Exception ex)
                {
                    //Console.WriteLine(string.Format("Esperando Acceso a log 1 ({0})segundos {1}", sleepTime, ex.Message));
                    try
                    {
                        log.Debug(string.Format("Resultado {0}: Tiempo: {1}{2}:{3}", file, executionDuration.Hours, executionDuration.Minutes, executionDuration.Seconds), true);
                        WriteLog = true;
                    }
                    catch (Exception exInt)
                    {
                        Console.WriteLine(string.Format("Esperando Acceso a log 2 ({0})segundos {1}", sleepTime, exInt.Message));
                        Thread.Sleep(sleepTime);
                    }
                }
            }
        }
    }
}
