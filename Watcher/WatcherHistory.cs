﻿namespace AMZFileSystemWatcherDaemon_v2.Watcher
{
    public class WatcherHistory
    {
        public System.DateTime Date { get; set; }
        public string FileName { get; set; }

        public WatcherHistory(string file)
        {
            FileName = file;
            Date = System.DateTime.Now;
        }
    }
}