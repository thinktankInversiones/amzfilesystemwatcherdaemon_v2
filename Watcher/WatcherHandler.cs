﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace AMZFileSystemWatcherDaemon_v2.Watcher
{
    public class WatcherHandler
    {
        private static WatcherLog4Net log;
        private List<WatcherObject> listWatcher;
        private Queue<WatcherObject> queueWatcher;
        public List<WatcherHistory> watcherHistory { get; set; }

        private int numberProccess;
        public WatcherHandler(int maxConcurrentProcess, WatcherLog4Net logg)
        {
            watcherHistory = new List<WatcherHistory>();
            numberProccess = maxConcurrentProcess;
            log = logg;
            listWatcher = new List<WatcherObject>();
            queueWatcher = new Queue<WatcherObject>();
        }

        public void addProccess(string fullFileName)
        {
            try
            {
                string folderPending = Directory.GetParent(fullFileName).ToString();
                //si entre la lista de watchers ya esta la carpeta actual, no es necesario hacer nada
                if (listWatcher.Any(i => i.PendingPath == folderPending))
                {
                    log.Debug("WatcherHandler Folder is adding in pending List " + folderPending);
                    WatcherObject watcher = listWatcher.First(i => i.PendingPath == folderPending);
                    bool yaEnEjecucion = false;
                    if (watcher.inExec.Contains(fullFileName))
                        yaEnEjecucion = true;
                    watcher.AddProductLoadsWatch(fullFileName, watcherHistory);
                    //si es fichero es fraccionado se ejecuta sin esperar a que termine al anterior (los isbn no se repiten), no puede haber uno anterior no fraccionado
                    //(revisar amzfileprocessor)
                    string[] filesParalell = ConfigurationManager.AppSettings["ficheroParalelo"].Split(';');
                    bool paralelo=false;
                    foreach (string filename in filesParalell)
                    {
                        if ((!yaEnEjecucion) &&(fullFileName.ToUpper().Contains(filename.ToUpper())))
                        {
                            paralelo = true;
                            break;
                        }
                    }

                    if (paralelo)
                    {
                        watcher.instanciasAbiertas++;
                        watcher.executeNext();
                    }
                }
                else
                {
                    if (queueWatcher.Any(i => i.PendingPath == folderPending))
                    {
                        log.Debug("WatcherHandler Folder is adding in pending Queue " + folderPending);
                        WatcherObject watcher = queueWatcher.First(i => i.PendingPath == folderPending);
                        watcher.AddProductLoadsWatch(fullFileName, watcherHistory);
                    }
                    else
                    {
                        WatcherObject watcher = new WatcherObject(folderPending, log);
                        watcher.AddProductLoadsWatch(fullFileName, watcherHistory);
                        if (listWatcher.Sum(o => o.instanciasAbiertas) < numberProccess)
                        {
                            watcher.executeNext();
                            listWatcher.Add(watcher);
                        }
                        else
                        {
                            queueWatcher.Enqueue(watcher);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error addProccess " + ex.Message);
            }
        }

        public void updateProccessStatus()
        {
            try
            {

                List<WatcherObject> localListWatcher = new List<WatcherObject>();
                //localListWatcher.AddRange(listWatcher);
                foreach (WatcherObject watcher in listWatcher)
                {
                    if (!watcher.isFreeProductLoadsWatch())
                        localListWatcher.Add(watcher);
                }
                listWatcher.Clear();
                listWatcher.AddRange(localListWatcher);
                ////se comprueba que no se mate alguno del os procesos en ejecucion
                //System.Diagnostics.Process[] processes = System.Diagnostics.Process.GetProcessesByName(Path.GetFileNameWithoutExtension(ConfigurationManager.AppSettings["ExecutablePath"].ToString()));

                //if ((processes.Length>0)&&(processes.Length != listWatcher.Count))
                //{
                //    log.Debug("El numero de ficheros <" + Path.GetFileName(ConfigurationManager.AppSettings["ExecutablePath"].ToString()) + "> en ejecucion <" + processes.Length + "> es distinto del numero de elementos de la lista <" + listWatcher.Count + ">", true);
                //    List<WatcherObject> removeListWatcher = new List<WatcherObject>();
                //    foreach (System.Diagnostics.Process p in processes)
                //    {
                //        int a = p.Id;
                //    }
                //    foreach (WatcherObject watcher in listWatcher)
                //    {
                //        if (!processes.Any(i => i.Id == watcher.PID))
                //        {
                //            removeListWatcher.Add(watcher);
                //        }
                //    }
                //    foreach (WatcherObject watcher in removeListWatcher)
                //    {
                //        listWatcher.Remove(watcher);
                //    }
                //}
                while ((queueWatcher.Count>0)&&(listWatcher.Sum(o => o.instanciasAbiertas) < numberProccess))
                {
                    if (queueWatcher.Count > 0)
                    {
                        WatcherObject watcher = queueWatcher.Dequeue();
                        log.Debug("Se activan ficheros encolados en "+watcher.PendingPath, true);
                        watcher.executeNext();
                        listWatcher.Add(watcher);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error updateProccessStatus " + ex.Message);
            }

        }

        internal void printStatus()
        {
            int numHoras = 48;//int.Parse(System.Configuration.ConfigurationManager.AppSettings["NumHorasControl"]);

            Console.ForegroundColor = ConsoleColor.Red;
            log.Debug(
                "\n***********************************Procesados ultimas("+numHoras+")horas*******************************************", true);

            var orderList = watcherHistory.Where(o => o.Date >= DateTime.Now.AddHours(-numHoras)).OrderBy(o => o.FileName).ToList();
            foreach (WatcherHistory watcher in orderList)
            {
                log.Debug(watcher.FileName + " <" + watcher.Date+">",true);
            }

            log.Debug(
                "\n******************************************************************************",true);
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Yellow;
            log.Debug(
                "\n***********************************Encolados*******************************************",true);

            foreach (WatcherObject watcher in queueWatcher)
            {
                log.Debug(watcher.PendingPath,true);
                foreach (string fileName in watcher.productLoadsWatcher)
                    {
                        log.Debug(DateTime.Now.ToString("g")+": " +fileName, true);
                    }
            }

            log.Debug(
                "\n******************************************************************************",true);
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Green;
            log.Debug(
                "\n***********************************Pendientes*******************************************",true);

            foreach (WatcherObject watcher in listWatcher)
            {
                log.Debug(watcher.PendingPath,true);
                foreach (string fileName in watcher.productLoadsWatcher)
                {
                    log.Debug(DateTime.Now.ToString("g") + ": " + fileName, true);
                }
            }

            log.Debug(
                "\n******************************************************************************",true);
            Console.ResetColor();


            //System.Diagnostics.Process[] processes = System.Diagnostics.Process.GetFileNameWithoutExtension(Path.GetFileName(ConfigurationManager.AppSettings["ExecutablePath"].ToString()));
            //if (processes.Length != listWatcher.Count)
            //{
            //    Console.ForegroundColor = ConsoleColor.Blue;
            //    log.Debug("El numero de ficheros <"+ Path.GetFileName(ConfigurationManager.AppSettings["ExecutablePath"].ToString()) + "> en ejecucion <" + processes.Length + "> es distinto del numero de elementos de la lista <" + listWatcher.Count + ">", true);

            //    foreach (WatcherObject watcher in listWatcher)
            //    {
            //        if (!processes.Any(i => i.Id == watcher.PID))
            //        {
            //            log.Debug("El siguiente Watcher no se encuentra en ejecucion", true);
            //            log.Debug(watcher.PendingPath, true);
            //            foreach (string fileName in watcher.productLoadsWatcher)
            //            {
            //                log.Debug(DateTime.Now.ToString("g") + ": " + fileName, true);
            //            }
            //        }

            //    }

            //    Console.ResetColor();
            //}

        }
    }
}